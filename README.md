# CoinMarketCap Dark Theme
A darker/grunge overhaul of the very popular CoinMarketCap.com.

CSS code for use on CoinMarketCap.  

## Easy Install
Download and install the Stylish extension:  
Chrome: https://chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe  
Firefox: https://addons.mozilla.org/en-US/firefox/addon/stylish/  
Opera: https://addons.opera.com/nb/extensions/details/stylish/  

Install the style: https://userstyles.org/styles/152205/coinmarketcap-dark-theme  

## Preview of Current Code
![CoinMarketCap](https://jaany.xyz/i/cmcMain.png)
![CoinMarketCap](https://jaany.xyz/i/cmcBTC.png)
